# Indieroad

This is the Hugo theme I developed for my blogs and websites. It originally derived from the [Mainroad theme](https://github.com/Vimux/Mainroad), but since then, no line of code is like it was before.

A lot of things are customized to [my own needs](https://jlelse.dev), but I wanted to open source the code, because it might be helpful to others to see how I implemented things.

This theme is released to the public domain, so you can do whatever you want with it. But of course it would be nice, if you give attributions and contribute improvements. Thanks!